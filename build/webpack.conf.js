const webpack = require('webpack')
const path = require('path')
const nodeExternals = require('webpack-node-externals')
const StartServerPlugin = require('start-server-webpack-plugin')
const Dotenv = require('dotenv-webpack')

const webpackPoll = 'webpack/hot/poll?1000'
const envPath = 'env/dev.env'

module.exports = {
  entry: [webpackPoll, './main'],
  watch: true,
  target: 'node',
  externals: [nodeExternals({ whitelist: [webpackPoll] })],
  module: {
    rules: [{ test: /\.js?$/, use: 'babel-loader', exclude: /node_modules/ }]
  },
  plugins: [
    new StartServerPlugin('server.js'),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new Dotenv({
      path: envPath
    })
  ],
  output: { path: path.join(__dirname, '../dist'), filename: 'server.js' }
}
