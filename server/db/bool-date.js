import Sequelize, { DataTypes } from 'sequelize'

/**
 * A bool date is a column that is initially NULL when false,
 * and is set to the date when it is to take effect.
 *
 * For example,
 * - column `RETIRED` is null, so it is not retired.
 * - column `HIATUS` is `2017-01-01` so it is true, and has been since then.
 *
 * The purpose of this file is to make it easier to supply the sequelize arguments
 * for such a column.
 */
function boolDate() {
  return {
    type: DataTypes.DATE,
    allowNull: true,
    defaultValue: null
  }
}

export { boolDate }
