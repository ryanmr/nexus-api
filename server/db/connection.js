import Sequelize, { DataTypes } from 'sequelize'

const config = {
  dialect: 'mysql',
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  options: {}
}

const db = new Sequelize(config)

export {
  db
}