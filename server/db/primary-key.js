import Sequelize, { DataTypes } from 'sequelize'

/**
 * Exports the sequelize object for a typical auto-increment primary key integer field.
 */

function primaryKey() {
  return {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  }
}

export {
  primaryKey
}
