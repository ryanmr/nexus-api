import express from 'express'
import { db } from './db/connection'
import { GeneralService } from './services/general-service'

db.sync({ logging: console.log })

const app = express()

app.get('/series', async (req, res) => {
  res.json(await GeneralService.findSeries())
})

app.get('/episodes', async (req, res) => {
  res.json(await GeneralService.findEpisodes())
})

app.get('/people', async (req, res) => {
  res.json(await GeneralService.findPeople())
})

app.get('*', (req, res) => {
  res.json({ big: 'week' })
})

export default app
