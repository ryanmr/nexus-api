import Sequelize from 'sequelize'
import { Series, SeriesMeta, Episode, Person, EpisodeRelation, EpisodePerson } from '../models'

const GeneralService = {
  findSeries() {
    return Series.findAll({
      include: [
        {
          as: 'meta',
          model: SeriesMeta
        }
      ]
    })
  },

  findEpisodes() {
    console.log('== before query in findEpisodes')
    console.log(Episode, Episode.prototype, Episode.Instance)
    return Episode.findAll({
      order: [['created_at', 'asc']],
      include: [
        {
          model: Series
        },
        {
          as: 'parent',
          model: EpisodeRelation
        },
        {
          model: Person
        }
      ]
    })
  },

  findPeople() {
    return Person.findAll()
  }
}

export { GeneralService }
