import Sequelize, { DataTypes } from 'sequelize'
import { db } from '../db/connection'
import { boolDate } from '../db/bool-date'
import { primaryKey } from '../db/primary-key'

const AlbumArt = db.define(
  'album_art',
  {
    id: primaryKey(),
    name: DataTypes.STRING,
    versions: {
      type: DataTypes.JSON 
    }
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    tableName: 'album_art'
  }
)

export { AlbumArt }
