import Sequelize, { DataTypes } from 'sequelize'
import { db } from '../db/connection'

const Cat = db.define(
  'cat',
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.STRING
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    tableName: 'cat'
  }
)

export { Cat }
