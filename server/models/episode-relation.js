import Sequelize, { DataTypes } from 'sequelize'
import { db } from '../db/connection'
import { boolDate } from '../db/bool-date'
import { primaryKey } from '../db/primary-key'
import { Episode } from './episode'

const EpisodeRelation = db.define(
  'episode',
  {
    id: primaryKey(),
    type: DataTypes.STRING,
    episode_id: DataTypes.INTEGER,
    episode_related_id: DataTypes.INTEGER
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    tableName: 'episode_relations'
  }
)

Episode.belongsToMany(Episode, {
  as: 'parent',
  through: EpisodeRelation,
  foreignKey: 'episode_id'
})
Episode.belongsToMany(Episode, {
  as: 'child',
  through: EpisodeRelation,
  foreignKey: 'episode_related_id'
})

export { EpisodeRelation }
