import Sequelize, { DataTypes } from 'sequelize'
import { db } from '../db/connection'
import { boolDate } from '../db/bool-date'
import { primaryKey } from '../db/primary-key'

const Person = db.define(
  'person',
  {
    id: primaryKey(),
    name: DataTypes.STRING,
    slug: DataTypes.STRING,
    email: DataTypes.STRING,
    urls: {
      type: DataTypes.JSON,
    },
    content: DataTypes.TEXT,
    teaser: DataTypes.STRING,
    is_core: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    tableName: 'people'
  }
)

export { Person }
