import Sequelize, { DataTypes } from 'sequelize'
import { db } from '../db/connection'
import { boolDate } from '../db/bool-date'
import { primaryKey } from '../db/primary-key'

const Media = db.define(
  'media',
  {
    id: primaryKey(),
    name: DataTypes.STRING,
    mime: DataTypes.STRING,
    type: DataTypes.STRING,
    length: DataTypes.INTEGER,
    size: DataTypes.INTEGER,
    url: DataTypes.STRING,
    hidden: boolDate(),
    meta: {
      type: DataTypes.JSON
    }
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    tableName: 'media'
  }
)

export { Media }
