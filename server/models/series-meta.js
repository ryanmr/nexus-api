import Sequelize, { DataTypes } from 'sequelize'
import { db } from '../db/connection'
import { boolDate } from '../db/bool-date'
import { primaryKey } from '../db/primary-key'

const SeriesMeta = db.define(
  'series-meta',
  {
    id: primaryKey(),
    series_id: DataTypes.INTEGER,
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    tableName: 'series_metas'
  }
)

export { SeriesMeta }
