import Sequelize, { DataTypes } from 'sequelize'
import { db } from '../db/connection'
import { boolDate } from '../db/bool-date'
import { primaryKey } from '../db/primary-key'
import { Series } from './series'

const Episode = db.define(
  'episode',
  {
    id: primaryKey(),
    series_id: DataTypes.INTEGER,
    title: DataTypes.STRING,
    subtitle: DataTypes.STRING,
    number: DataTypes.INTEGER,
    short_description: DataTypes.STRING,
    long_description: DataTypes.TEXT,
    content: DataTypes.TEXT,
    docs_link: DataTypes.TEXT,
    hidden: boolDate(),
    unlisted: boolDate(),
    nsfw: DataTypes.BOOLEAN,
    published_at: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null
    }
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    tableName: 'episodes'
  }
)

Episode.belongsTo(Series, {
  foreignKey: 'series_id'
})

Series.hasMany(Episode, {
  foreignKey: 'series_id'
})

export { Episode }
