import Sequelize, { DataTypes } from 'sequelize'
import { db } from '../db/connection'
import { boolDate } from '../db/bool-date'
import { primaryKey } from '../db/primary-key'
import { SeriesMeta } from './series-meta'

const Series = db.define(
  'series',
  {
    id: primaryKey(),
    name: DataTypes.STRING,
    slug: DataTypes.STRING,
    long_slug: DataTypes.STRING,
    short_description: DataTypes.STRING,
    long_description: DataTypes.TEXT,
    hidden: boolDate(),
    retired: boolDate(),
    hiatus: boolDate()
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    tableName: 'series'
  }
)

Series.hasOne(SeriesMeta, {
  as: 'meta',
  foreignKey: 'series_id'
})

export { Series }
