import Sequelize, { DataTypes } from 'sequelize'
import { db } from '../db/connection'
import { boolDate } from '../db/bool-date'
import { primaryKey } from '../db/primary-key'
import { Episode } from './episode'
import { Person } from './person'

const EpisodePerson = db.define(
  'episode_people',
  {
    id: primaryKey(),
    role: DataTypes.STRING,
    episode_id: DataTypes.INTEGER,
    person_id: DataTypes.INTEGER
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    tableName: 'episode_people'
  }
)

Episode.belongsToMany(Person, {
  through: EpisodePerson,
  foreignKey: 'person_id'
})
Person.belongsToMany(Episode, {
  through: EpisodePerson,
  foreignKey: 'episode_id'
})

export { EpisodePerson }
